require.config({
	paths: {
		jQuery:'/js/lib/jquery',
		Underscore: '/js/lib/underscore',
		Backbone:'/js/lib/backbone',
		text:'js/lib/text',
		templates:'../templates',
		Sockets: '/js/lib/socket.io',
		models:'/models',

		SocialNetView : '/js/SocialNetView'
	},

	shim: {
		'Backbone':['Underscore','jQuery'],
		'SocialNet':['Backbone']
	}
});

require(['SocialNet'],function(SocialNet){
	SocialNet.initialize();
});