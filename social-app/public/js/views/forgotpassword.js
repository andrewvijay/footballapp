define(['text!templates/forgotpassword.html'], function(forgotpasswordTemplate){
	var forgotpasswordView = Backbone.View.extend({
		el: $('#content'),

		events: {
			"submit form" : "password"
		},

		password: function(){
			$.post('/forgotpassword',{
				email: $('input[name=email]').val()
			}, function(data){
				console.log(data);
			
			});
			return false;
		},

		render: function(){
			his.$el.html(forgotpasswordTemplate);
		}
	});

	return forgotpasswordView;
});